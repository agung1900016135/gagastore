<?php
class voucher_model extends CI_Model{
    public $id_voucher;
    public $nama_voucher;
    public $jumlah_voucher;
    public $harga_voucher;

    public function getgagavoucher()
    {
        $this->load->database();
        $voucher = $this->db->get("voucher");
        $result = $voucher->result();
        return json_encode($result);
    }
}